#!/bin/bash

gsettings set org.gnome.desktop.input-sources xkb-options "['compose:ralt']"

# keybindings
gsettings set org.gnome.desktop.wm.keybindings switch-input-source "['<Shift>Shift_R']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-left "['<Primary>Left']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-right "['<Primary>Right']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-up "['<Primary>Up']"
gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-down "['<Primary>Down']"

gsettings set org.gnome.desktop.wm.preferences focus-mode 'mouse'
gsettings set org.gnome.desktop.wm.preferences auto-raise true
gsettings set org.gnome.desktop.wm.preferences auto-raise-delay 1000

# Ubuntu only - abandoned
# gsettings set com.canonical.desktop.interface scrollbar-mode normal

